## Warum habe ich immer Schmerzen beim Sex?

<div class="handwritten">
Nächste Frage.
Warum habe ich immer Schmerzen, wenn ich mit meinem Mann zusammen bin.
Den Männern scheint es nichts auszumachen.
Ich habe nie einen Mann sagen hören, dass es weh tut.
Ich kann den Akt an sich genießen, aber dann bekomme ich wie einen Krampf.
Und wie eine unangenehme Spannung innen.
Und manchmal bekomme ich schon von Anfang an Angst, dass er kommen könnte.
Was soll ich tun?
</div>

Es klingt nach etwas Neurologischem.
Dass es etwas in deinem Körper ist, was ausgelöst wird, und was diese Probleme schafft.
Ich denke, dass jemand der Experte ist am Nervensystem, dir helfen kann.
So gehe in erster Linie zu einem solchen Arzt und untersuche die Sache.
Dann kannst du zu mir zurückkommen und wir können sehen, ob es ein Konflikt gibt oder eine Erinnerung in dir gibt, die die Sache verwickelt.
Aber zuerst das Physische untersuchen lassen.

<div class="handwritten">
Neue Frage:
Wenn ich mit meinem Mann schlafe, dann tut es nur weh.
Ich tue nichts als diese Schmerzen spüren und den Krampf in meinem ganzen unteren Leib.
Und das ist nur eine Plage.
Wie kann ich da rauskommen?
</div>

Ich habe diese Frage berührt und bin vorhin darauf eingegangen, als ich den Fall früher beschrieben habe.
Aber kurz gesagt brauchst du zum einen vielleicht etwas Krampflösendes einzunehmen und Selbststimulierendes,sodass der Krampf weg ist, wenn du mit einem Mann zusammen bist.
Zum anderen brauchst du zu verstehen, dass Angst dahinter ist.
Und dich vielleicht in der Kunst erüben, zu wissen, dass es ungefährlich ist, dass du dich schützen kannst davor schwanger zu werden, und dass du dich gegenüber Geschlechtskrankheiten schützen kannst dadurch, dass du Kondome verwendest.
Und dadurch ist es dir vielleicht möglich, dich zu entspannen.
Zu wissen dass Sex ein Spielen ist, und dass es oft angenehm wird bald.
Mach Gebrauch von deinen Sinnen und erlebe die Gefühle des anderen.
Und gebe dich hinein in sie.
Und dann vielleicht, vielleicht wird der Krampf gelindert.

Manchmal kann es daran liegen, dass wir als wir Kinder waren, im Krampf waren nicht zu kackern, weil wir meinten, dass wir kaputt gingen oder etwas verlieren würden, wenn wir kackern würden.
Und dass die Freude bei der Mutter oder deinem Vater hervorgerufen hat, und diese Freude, die sollten sie gewiss nicht bekommen.
Wir sind wie eingesauert darin.
So vernehme, was.

