## Warum haben Jungs so viel Angst vor starken Frauen?

<div class="handwritten">
Nächste Frage:
Warum sind manche Jungs so viel Angst vor starken Frauen?
Warum werden die kraftvollen Frauen so oft alte Jungfern, und finden niemanden mit dem sie das Leben teilen?
Liegt das an ihnen oder ist etwas schief in der Zivilisation?
</div>

Um dem Trieb Sexualität gibt es so viele Mythen.
Und am Anfang bauten sie auf die Angst von jemandem.
Auffassungen von Frauen wie das schwache Geschlecht, dass man Frauen kurz halten braucht, dass Frauen ihren Platz wissen sollen, dass Frauen, die mehr als drei Meter vom Herd sind, weggelaufene Frauen sind, dass andere Männer Frauen, die in einer Beziehung sind, erobern wollen, weil es Status gibt.
Und tausend andere Vorstellungen, die aus der Bibel oder aus alten Texten von Göttersagen oder lehrreichen Erzählungen, Fabeln und so weiter geholt sind.
Diese sind erzählt den meisten Kindern.
Und Kinderfilme enthalten oft solche Stereotypen.
Und daher bekommen Kinder Bilder, wie es sein soll.
Und diese finden ihren Platz, wenn Kinder intensiv sich entwickeln, wenn sie geschlechtsreif werden.

Um deine Frage zu beantworten, braucht man den Ausgangspunkt von dem Grundgedanken bei dem Patriarchat zu verstehen.
Davor wusste man immer, wer die Mutter war, und hat sich damit zufrieden gegeben.
Vater waren alle Männer, die in der Nähe waren.
Damals gab es nicht dieses Problem, was du ansprichst.
Aber als es wichtig wurde, dass Gott ein Mann ist, und dass der Mann sein Repräsentant auf Erden ist, da wurde es wichtig zu wissen, wer der Vater war.
Und das war nicht ganz leicht.
Deshalb entstand so viel Misstrauen und Angst um starke Frauen herum, die eigenmächtig sich nicht darum geschert haben, sich nur an einen Mann zu halten.
Sowie dass die Männer mit allen Frauen schlafen wollten, damit ihre Egogene so weite Verbreitung wie nur möglich erhalten sollten.
Wenn du bei dem suchst, bekommst du vermutlich eine Antwort auf deine Frage.
