## Tabus und Totems

<div class="handwritten">
Was ich dabei bemerke, wenn du das sagst, ist, dass die Sexualität überall und in allen Situationen ist und sie wird auf verschiedene Weise eingekreist. 
Es gibt sie nie frei strömend als der Trieb, den sie zu sein scheint. 
Ich frage mich, wie sie aus sich heraus sein würde.
</div>

In den meisten Ursprung-Kulturen gibt es Totem und Tabu. Das heißt, dass etwas da ist, was das Leben schützen soll und es gibt gegen manche Sachen, die schädlich sind Verbote. Um sie zu verstehen braucht man vertraut zu sein mit den Sitten und Gebräuchen eines Stammes, wie er organisiert ist, wie er geführt wird und welche Mittel es gibt um den Stamm zu lenken. Ich habe vor ein Beispiel einer solchen Kultur anzusprechen.

Aber halte in Erinnerung, dass jede Herde, jede Schar ihre eigenen Totems und Tabus hat und es deshalb nicht möglich ist, zu verallgemeinern. Man kann es nur als eine von vielen Vorstellungen haben.

Eine Herde bildete sich auf einen Platz, wo es Voraussetzungen zum Leben gab. Das heißt, es gab für die konstante Herde zum Essen. Die Gruppe erwählte einen Dorfältesten, einen Medizinmann, Schaman oder sowas. Das ist jemand, der den Ausschlag bei einer Streitigkeit fällt. Der übliche Weg zu Entscheidungen war Konsensus. Wenn niemand dagegen war, konnte man es durchführen. Aber wenn das nicht möglich war... fanden Verhandlungen über eine gewisse Zeit statt, und wenn es dann trotzdem nicht möglich war, zum Entschluss zu kommen, dann war es jemand, der klug und der erwählt worden war, der die Entscheidung treffen musste.

Die Männer war eine Gruppe die ging und kam. Sie jagten größere Beutetiere, wo sie miteinander zusammenwirken brauchten, um das Beutetier erlegen zu können. Auch mussten sie miteinander zusammenwirken, um schlafen zu können, und um in der wilden Natur klarzukommen.
(Ein Rest von dieser Männer-Herden-Mentalität gibt es noch beim Sport. Zum Beispiel ist eine Fußballmannschaft ein typisches solches Überbleibsel.)
Sie verließen die Siedlung und machten sich auf den Weg in das nächste Dorf und da entstand Freude und Feierlichkeit. Sie brachten gutes Essen mit und diese Gäste wurden mit der Gunst der Frauen verehrt. Leute im Dorf, Frauen, Kinder, die Gebrechlichen und die nicht arbeiten konnten, sie legten ein Feuer auf dem Platz mitten im Dorf an und ließen es brennen, um die wilden Tiere fern zu halten. Die Leute tanzten um dieses Feuer, sie stampfen und sie machten Laute, um gefährliche kleinTiere auf Abstand zu halten, Schlangen, Skorpione, Spinnen und andere Tiere. 
Es entstand ein Bereich der Geborgenheit. Kinder, Alte und diejenigen, die nicht arbeiten konnten, zogen sich zurück in die Schlafscheune und die Männer und die Frauen fuhren mit dem Singen, Tanzen und Essen fort. Und diese äußere Geborgenheit machte es möglich, in der inneren geborgenen Trance zu sein. Da gedeihte die Erotik und die Sexualität kam auf und wurde die Tage geübt, als dies in gang war. Es war der Vermehrungsakt. Bei dem gab es keine die Zweisamkeit, denn es war nicht sicher, dass diese Männer wiederkehren würden. Die Teilnehmer übten diese Orgie und neun Monate später wurde eine Anzahl von Kindern geboren.
Da die Mutter für das Kind sorgte, wusste man wessen Kind es war und wenn sie starb, war es ein Kind vom Dorf und jemand anders sorgte für es.

Die Männer verließen danach das Dorf und zogen weiter und vielleicht gelangten sie in ein anderes Dorf, wo sie das Ritual wiederholten. Wenn diese Gruppe von Männern ankam, hatten sie vielleicht lange nichts zum Essen gehabt und sie hatten nötig, zu essen, zu schlafen, und versorgt zu werden.
Da war es wichtig, dass keine Kinder und keine alten Leute in der Nähe waren.
Denn es war für sie tabu, daran teilzunehmen. Sie hielten sich fern, damit keine Lüsternheit auf Kinder und nicht geschlechtsreife jungen Menschen aufkommen würde. Auch war das so, dass diese Männer Kinder schlachten und aufessen konnten, weil sie keine Beziehung zu ihnen hatten.

Wer gegen dieses Verbot verstieß, wurde in die wilde Natur hinaugeworfen und das war gleichbedeutend mit dem Tod. Es war ein Tabu, dass man einfach nicht brechen durfte.
Diesen Männern, die die Herde verließen, wurde ein Talisman mitgegeben, etwas, woran sie sich halten konnten, um bei Sinnen zu bleiben, damit die Angst sie nicht blind machte, um in innerer Geborgenheit zu bleiben. Wohin sie zogen, wusste niemand.

Oft gab es einen Totempfahl. Er war dazu da, mit allen bösen Kräften klarzukommen, die diese Zeremonien umgaben, und er sollte vor schnellem grausamem Tod bei den Kupplungsgelegenheiten schützen.

Wenn diese Männer wegzogen, nahmen sie die Jungen mit, die gerade dabei waren, geschlechtsreif zu werden und sie wurden eingegliedert, um mit den männlichen Ritualan vertraut zu werden. Die Gruppe ließ auch einige zurück, die nicht mehr gebraucht wurden und es waren die, die älter waren und nicht mehr die Kraft hatten, sehr aktiv beim Jagen dabeizusein. Und diese wurden im Dorf für die Herde gebraucht, um Zäune um die Wohnhäuser zu bauen, oder um Wohnhäuser zu bauen, unter anderem. Ihr Beitrag war im Ganzen sehr wichtig. 

Bei manchen Stämmen durften diese Männer bei verschiedenen Frauen schlafen, um so ihre sexuelle Fähigkeit, aufrecht zu erhalten. Das war auch günstig für das Wachstum der Gruppe.
Auch wenn viele Kinder geboren wurden, so waren es viele Kinder, die vor dem Alter von vier Jahren starben, und daher wurden es nie zu viele.
