## Es gibt nur einen Gott, und er ist ein Mann

Diese Standespersonen waren Imperialisten und ihr Machtgebiet war große Teile des Mittleren Ostens und des Nordafrikas, darunter Israel, wo das Judentum war.

Der Unterschied lag darin, dass das Judentum ein Ein-Gott-System war und dass dieser Gott ein ER war. Dieser Gott wurde durch Männer vertreten und es waren die Männer, die Menschen waren. Die Frauen waren nur Frauen und etwas anderes als Männer, und ihnen kam keine weitere Bedeutung zu, als Besitztümer der Männer zu sein und den Männern Kinder zu gebären, sowie der Mann sicher wissen wollte, dass er der Vater des Kindes war. Und deshalb entstand die Monogamie, in der Art wie wir sie kennen und wie sie herausgearbeitet worden ist.
Das heißt, dass die anonyme Frau ihrem Herrn treu bleiben soll und keine anderen Männer außer ihm haben soll. Und wenn Männer dann sich bei ihr durchsetzen, dann ist es ihre Schuld.
Und an diesem Punkt wurde das Menschliche in einer ungesunden Weise gespalten und die Sexualität wurde zu einer geladenen Streitfrage.
Das Misstrauen gegenüber anderen Männern veranlassten die Männer, ihren Besitztum, ihre Frau, schützen zu müssen und daher kommen alle die Beschränkungen, die wir heute noch sehen, in Form von, dass Frauen nicht ihre Haare zeigen dürfen und Kleider, die bis zu ihren Füßen reichen, tragen müssen, wie es in einigen Religionenvarianten, die von der jüdischen Lehre ausgehen, der Fall ist.

Da Männer nicht nur mit ihren untergebenen, passiven Frauen zufrieden waren, entstand die Prostitution, und diese wurde als sündenhaft abgestempelt...
nicht die Männer, aber die Frauen, die auf diese Weise ausgenutzt wurden, waren sündenhaft.
Die ganze Ein-Gott-Religion schuf die menschlichen Gesetze zu den Bedingungen der Männer.
Die Männer entschieden, was in den Augen Gottes Sünde war und was erlaubt und fromm war.

Sex wurde zu einem Produkt, das man kaufen und verkaufen konnte, und er wurde zu etwas Exaltiertem erhoben, das alle andere Vergnügungen übertreffen sollte. Frauen wurden zu Waren, die man kaufte und verkaufte - aber nicht die gesamte Frau, sondern ihr Geschlecht.

Es wurden geheime Sekten gegründet, die eine eine Menge sonderbare Bündnisse schlossen, sogar mit dem Teufel, Hexentanz und so weiter. Die Sexualität wurde mystifiziert und sie wurde zu etwas anderem als ein Sinnliches erhöht. Und die Sexualität wurde zu einer Art von geistiger, heiliger Kuh aus der man das Beste herausmelken sollte.
Die Frau sollte den Mann befriedigen und wenn sie das nicht tat, hatte er das Recht, sie zu verprügeln, sie zu verkaufen und sie zu töten. Einen Rest davon haben wir bei der 'Gewalt in Nahbeziehungen', wie man es heutzutage nennt.

Dieser Trieb wurde auch zu etwas Göttlichem erhoben, wie wenn Männer ohne ihn nicht auskämen, wie wenn dieses Ohne-Auskommen schädlich war und das gerechtfertigte ihr Anrecht, Frauen als Objekten ihren Willen mit Gewalt aufzuzwingen... später auch bei Kindern und anderen Menschen, die in Abhängigkeit waren. Beachte (!) den Skandal, der nach und nach ans Licht gekommen ist, bei dem katholische Priester, jahrzehntelang die Knaben der Kirche sexuell ausgenutzt haben, weil es nicht in der Bibel steht, dass Gott das verboten hat. Die ganze #metoo-Bewegung handelt in einer modernen Version gerade vom diesem.
