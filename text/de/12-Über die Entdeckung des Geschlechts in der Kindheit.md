## Über die Entdeckung des Geschlechts in der Kindheit

Okay, jetzt kommen wir dazu, wirklich über die Sex-Probleme zu reden, die die Menschen haben, und welche ihnen so viel am Zusammenleben zerstört und dieses möchte ich gernen auf meine Weise spiegeln.
Ich hoffe, du stellst all' die Fragen, auf die du eine Antwort haben möchtest, denn jetzt weißt du etwas über meine Referenz, wenn du die Antwort hörst.

<div class="handwritten">
 Ja, es berührt mich, all das, was du bis jetzt gesagt hast.
Das ist mir ein bisschen komisch, denn im Grunde weiß ich davon und dennoch weiß ich es nicht.
Es ist so schwer, wenn die ganze Umgebung von einem etwas anderes sagt und tut und obwohl man es selber besser weiß, so tut man selber auch etwas anderes.
Ich verstehe, dass ich durch diese Kultur und durch die Menschen, die um mir sind sehr fest geprägt worden bin, aber dennoch finde ich, dass ich mehr mich selber sein sollte und sein müsste, und dieser existenziellen Angst nicht Opfer fallen, die so leicht durch die Sexualität eines anderen ausgelöst wird und die zu einem soo großen Problem in meinen nahen Beziehungen wird.
</div>

Okay.
Kannst du dich erinnern wie das war, als du selber klein warst.
Wann hast du angefangen, daran zu denken, dass Kinder unterschiedlicher Geschlechter waren,
dass sie unterschiedlich waren und dass man selber etwas anderes war als der andere.

<div class="handwritten">
 Es war als ich in der Schule anfing.
Davor im Kindergarten, was es uns allen egal, ob es Jungs oder Mädels waren, die miteinander gespielt haben.
Es gaben Jungs, die in der Puppenecke dabei waren, und es gaben sehr viele Mädchen, die Fußball gespielt und die miteinander gerungen haben und die bei den Fang-, Ball- und sonstigen Gruppenspielen dabei waren.
Aber oft waren es die Jungs, die über die Regeln entschieden, und ich weiß, dass ich dies sehr gern hatte.

Als wir dann in die Schule kamen, da sollten die Mädchen Zeit in der Sauna haben, wo die Mädchen nur unter sich waren, und die Jungs sollten zu anderer Zeit da sein.
Wir sollten vor dem Turnen zum Umkleiden getrennte Zimmer haben.
Und wir wurden bei verschiedenen Gelegenheiten zusammengepfercht, so dass die Mädchen unter sich und die Jungs unter sich waren.
Da weiß ich, dass ich anfing, die Jungs mit anderen Augen anzuschauen als ich es früher getan habe... dann... ganz plötzlich, wurde etwas Fremdes ins Menschsein, ins Kindsein, es entstanden zwei verschiedene Gruppen, Jungs und Mädchen.
Ich glaube nicht, dass mir klar wurde, dass es mit dem Geschlecht zu tun hatte.
Aber irgendwie sollte man auf eine gewisse Weise sein, wenn nur Mädchen da waren, als wenn sowohl Mädchen als auch Jungs da waren.
</div>

Es ist das erste Zeichen der Entwicklung, dass einem bewusst wird, dass es zwei verschiedene Arten von Menschen gibt und dies hat in manchen Zusammenhängen eine Bedeutung hat, aber meistens hat es das nicht.
Wenn wir gemeinsam Dinge als Menschen tun, da hat es keine Bedeutung, nur kulturell werden sie getrennt.

Bei der Kameradschaft unter Kindern wird kein Unterschied zwischen Jungs und Mädchen gemacht, sondern sie sind beim Miteinander unverhindert.
Aber es gibt schon mädchenhafte Mädchen, die Jungsspiele nicht verstehen, und es gibt Jungs, die finden, dass Mädchenhaftigkeit lächerlich ist, und unheimlich irgendwie.
Die Frage ist, woher sie diese Vorurteile aufgenommen haben.
