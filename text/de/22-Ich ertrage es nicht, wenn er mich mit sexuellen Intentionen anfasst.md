## Ich ertrage es nicht, wenn er mich mit sexuellen Intentionen anfasst

<div class="handwritten">
Jetzt hätte ich gerne, dass meine Freundin ihre Frage stellen darf.
</div>

Das geht in Ordnung, bitteschön.

<div class="handwritten">
 Hallo Iris, ich finde, dass ich dir diese Fragen schon gestellt habe.
Aber ich finde nicht, dass du mich verstanden hast und mir eine Antwort gegeben hast.
Irgendwo weiß ich, dass wenn du nur nur verstehst, was ich frage, so wirst du mir eine Antwort geben, die mir sehr helfen kann.
Aber erst, wenn du an meine wirkliche Frage herankommst.
Ich finde, dass ich überaus deutlich gewesen bin, aber du verstehst trotzdem nicht.
</div>

Ich werde mein Bestes tun, um deine Fragen zu beantworten auf meine Weise, und vielleicht findest Du selber heraus, was du wissen brauchst, um die Werkzeuge zu bekommen, die du brauchst.
Bitteschön, die Zeit gehört dir.

<div class="handwritten">
Okay.
Ich finde, dass mein Therapeut es sehr gut beschrieben hat, das letzte Mal als wir dort waren.
Er sagte, „Frauen sind empfindlich, wenn der Mann zu ihnen kommt und eine Entladung sexuellerweise haben möchte.
Und manchmal dann finden sie, dass es so anstrengend ist, dass sie abschalten.
Und sie wollen, dass der Mann zu ihnen kommen soll und einfach offen sein soll.“
Da fühlte ich mich gesehen.
Genauso ist es bei mir.
Es tut mir weh im ganzen Körper.
Es entsteht wie ein Krampf.
Sobald es konkret wird.
Davor ist es ganz wunderbar,aber dann werde ich steif, innen, und ich schalte ab.
</div>

Das verstehe ich und es ist sicherlich gut, dass du dich darin wieder findest und dich verstanden fühlst.
Aber hilft dir das, dass es dir besser geht in deiner Beziehung mit deinem Mann, sexuellerweise?
Oder wie du sagst, du bist überempfindlich, weil er dich anfasst.
Dass dies einfach unangenehm ist und dass du es nicht aushältst.
Du kannst die Anziehung spüren, wenn er dich nicht berührt, aber sobald er dich berührt, bekommst du Angst und es wird unangenehm und du gerätst in einen Krampf und kommt nicht da raus.
Was macht, dass alle deine Lustgefühle verschwinden, und du wirst kalt wie ein toter Fisch.

<div class="handwritten">
 Nein, das ist wahr, das lindert bei mir irgendetwas, so dass ich nicht alle Schuld auf mich nehme, weil es nicht funktioniert.
Aber das hilft mir nicht mit dem Problem ansich.
Aber du hilfst mir auch nicht mit dem Problem.
Du beschreibst nur wie ich aushalten soll, du beschreibst, wie es ist, wenn man nicht diese Überempfindlichkeit hat.
Und manchmal geht es mir so und dass du einfach gemein bist, wenn du sagst, dass ich einfach ertragen muss, dass ich mich so fühle dass ich vergewaltigt werde.
Das ist so ohne Hoffnung, dass ich weiterhin einfach nach einer Nadel im Heu suchen brauche, dass es nichts gibt, was ich tun kann, dass ich mein Sexleben mit meinem Mann verbessern kann.
Dass ich einfach weiterhin ertragen muss, wie er mir weiterhin Angst einjagt.
Dass seine Hände unerträglichen Schmerz hervorrufen und dass ich nichts machen kann.
</div>

Oh, das ist deine Deutung, was ich gesagt habe, nicht das was ich sage.
Aber ich verstehe, dass es möglich ist, das so zu deuten, wenn du verzweifelt bist.
Verzweifel darüber, dass du keine brauchbare Antwort von mir bekommst.
Und ich werde versuchen, es so zu erklären, dass du bestenfalls es anders deuten kannst.
Ja, es ist wahr, dass du überempfindlich bist, dass du hineininterpretierst und nicht erträgst seine Fehler und Mängel, wenn er nur danach aus ist, Sex zu haben, um eine schöne Zeit zu haben.
Dass er dich anfassen will, damit du erotisch geladen wirst und du dadurch ihm entgegenkommen möchtest.
Dass er versucht, lieb und mitkommend zu sein.
Und auch wenn er all das tut oder versucht, dann ist der dennoch nur in der Situation, Samenerguss bekommen zu wollen, um sich entspannen zu können und einfach zu sein.

Er weiß nicht wie das geht, erst zum Sein zu kommen, und dann das zu bekommen, was er sich wünscht.
Er weiß nicht wie das geschieht, denn in ihm so gibt es das Sein die ganze Zeit.
Und deshalb versteht er nicht, wie er zu etwas kommen soll, in dem er schon ist.
Du bist nicht in dem unbedingt, du ruhst nicht in dem, sondern gleitest hinüber in das Sekundäre, Elementare und WWählbare.
Du gerätst in Bewertungen, bezogen auf sein Wünschen.
Dieser Wunsch ist wahr und an ihm ist nichts Falsches.
Wenn du dahinter gehen könntest und verstehen könntest, dass es Liebe ist, aber stattdessen fühlst du dich vergewaltigt.
Das Primäre ist da, wenn nichts anderes da ist.
Und es ist ein Sein, in dem er ruht, genauso natürlich wie wenn er atmet.
Und ihm wird das unbegreiflich.
Er kann nicht etwas schaffen, was schon da ist.
Dann wird es eine Anpassung, die bewirkt, dass er verliert, sich selber zu sein.
Und dann hat niemand von euch beiden etwas davon, was du dir wünschst.
Weder du noch er.

Das ist ein gewöhnliche Grund davon, dass Männer impotent werden.
In deiner Welt wird es wie: „Wenn er dies nur entfernen könnte, dann wäre es perfekt“, aber das ist nur ein Traum.
Es ist in etwa: „Wenn er nur verstünde, dann wäre das Problem gelöst“.
So ist es nicht, weil du möchtest, dass er anders sein soll als er ist.
Und du glaubst irgendwie, dass es im Bewusstsein ist, wo er dies verändern kann.
Und das ist es nicht.
Es ist auf einer viel tieferen Ebene, wo er das verändern kann.
Und wäre er imstande sich anzupassen um deinetwillen, dann würdest du einen teuren Preis zahlen.
Und ich weiß, dass du dies nicht möchtest.
Nicht wegen des Preises, sondern weil du trotzdem möchtest, dass er so sein soll wie er ist, wenn er sich selber ist.
Und weil du nicht möchtest, dass die Beziehung mit Teilen aufhört.

Kannst du verstehen, dass du Angst hast vor den Gefühlen, die in dir geweckt werden durch seine Hände, dass das ungefährlich ist und dass du brauchst hinter die Angst zu schauen.
Was es wirklich ist, was dir Angst macht.
Hast du in dir selber entdeckt, wenn wenn du durch Zuhören Hilfe bekommen hast, dass es daran liegt, dass er in irgendeiner Form von Ergebnis ist, dass er zum Erguss, zur Entladung kommen möchten möchte.
Dass er dorthin möchte, indem er Samenerguss bekommt dadurch, dass er mit dir schläft, in Liebe.
 
Aber was ist da hinter deiner Angst?
Wie ist sie entstanden am Anfang?
Hast du etwas Übergriffsmäßiges erlebt, was du alles Vergewaltigung interpretiert hast?
Oder kann das so sein, dass du irgendwann in der Pubertät als die Jungs mit dir knutschen wollten, einen Schock bekommen hast?
Oder ist es irgendeine Art der Schwierigkeit, der Kommunikation in dir?
Das heißt nicht, dass es dein Fehler ist, dass es an dir liegt.
Auch nicht, dass etwas bei dir nicht stimmt.
Es ist nur so, dass wir sehr verschieden sind und die Empfindlichkeit ist auch verschieden.
Und wenn wir vergleichen ist es so leicht, einen Sündenbock bei sich zu suchen oder bei dem Partner zu suchen, anstatt zu verstehen, dass es die Unvollständigkeit ist, die es mit den Idealen vermasselt.
Es ist wahr, dass du die Probleme bekommst, die du bekommst.
Aber du bist ihm kein Objekt.
Du bist die Person, die er am meisten liebt, die er liebt, Zeit mit dir zu verbringen, mit dir leben,  er hat dich gewählt als einen Partner und will dich.
Er meint,dass dies auch den Trieb umfasst.
Und für ihn ist das so einfach, dass man geil wird, man schläft miteinander, und dann kommt man vom Sein in einen entspannten Zustand.
Wenn du hörst, dass er keine Bewertungen bezüglich diesem hat.
Es ist für ihn einfach unreflektiert, wie wie das Atmen oder wie das Schlagen des Herzens.
Ist dies etwas, was du annehmen könntest, damit du keine Angst bekommst vor seinem Trieb?
Ich meine nicht, dass Du Dich anpassen sollst, einfach schauen, was daran so gefährlich ist, dass er unreflektiert in seinem Trieb ist, in seiner Sexualität ist, warum es dir Angst macht.

<div class="handwritten">
Immer noch so verstehst du mich nicht, du verstehst nicht, dass es unerträglich ist, und dass ich so viel Gewalt mir angetan habe, so viele Male schon.
Ich kann es einfach nicht mehr.
Was unternehme ich bloß damit, dass ich so überempfindlich bin.
So überempfindlich wie es anderen Menschen geht.
Du sagst darin gibt es ein Talent, aber ich erlebe das nur als eine Belastung.
Und wenn ich versuche, es ihm zu erklären, meinem Mann, dass es mir Angst macht, wenn er zu mir kommt, und seine sexuelle Frustration loswerden will, anstatt einfach zu sein, mit mir gemeinsam, dann versteht er nichts.
Er denkt, ja, aber das ist ja das, was ich tue.
Aber das ist es nicht.
Weil wenn er mich anfasst, anwesend, ohne diesem Ziel, dann entspanne ich mich.
Aber wenn er mich anfasst, weil er etwas möchte, dann bekomme ich Angst und ich würde verspannt.
Alles ist so sehr unangenehm.

Aber er bemerkt diesen Unterschied nicht.
Nur ich tue es.
</div>

Ich glaube nicht, dass es möglich ist, für einen Menschen das zu verstehen, der nicht diese Überempfindlichkeit hat, wie es ist, so überempfindlich zu sein gegenüber Berührung.
Aber ich finde, dass du es auf eine ausgezeichnete Weise gelöst hast.
Dass du ihn berührst wie er es gerne hat, und dass er es einfach empfängt, und er versteht dass du nicht das gleiche Angenehme erlebst wie er.
Und es tut ihm leid, deinetwegen.

Dann nehme ich das ernst an, dass du es auf die Weise hast, wie du es hast.
Ich meine, dass du irgendeine Form der Fibromyalgie hast.
Sie ist eine Autoimmunkrankheit, wo das sensitive Nervensystem in einen Krampf gerät.
Über ein Ängstesystem und das Problem an sich kann man besänftigen, indem man den Körper geborgen macht, so dass er^ nicht in dem Kampf sein braucht.
Und da braucht man anzuschauen welche die ungefährliche Angst ist, die Auslöser für den Krampf ist.
Man kann das gut bei der Primärarbeit machen.
Aber dann ist der Weg lang und braucht Zeit, sodass du auflösen und wieder auflösen brauchst bis die Überspannung nicht wiederkehrt.
Vielleicht ab und zu mal.
Ist das verständlich?

Gerade dies, dass es dir keine Hilfe ist, wenn er sich anpasst, und dies nicht bei dir auslöst, sondern dass du freistehend von ihm dich in der Kunst übst, hinter das Ängstesystem zu gehen und es dazu zu bringen, den Kampf nicht auszulösen.

<div class="handwritten">
 Aber ich sage ja, dass es nicht möglich ist, weil seine Art mich zu berühren, mir Angst macht.
Seine Intention fühlt sich unangenehm an auf meinem Körper.
Ich genieße das ganz und gar nicht.
Das ist ganz und gar unmöglich.
Es ist mir nur nur nur möglich gewesen mit Männern, die nicht so sind.
Dagegen habe ich es so lange ertragen, dass Männern mich mit der falschen Intention anfassen.
So so sehr, dass ich total allergisch darauf geworden bin.
Das ist einfach nicht mehr möglich.

Ich hasse Massage auch wegen diesem.
Du du sagst immer noch, dass ich etwas genießen soll,was so extrem unangenehm für mich ist, so dass es einfach nicht geht.
Das ist keine Hilfe.
</div>

Noch steckst du in deiner Interpretation.
Und sie hat Vortritt vor dem, was ich sage.
Manche Männer haben diese Sensitivität als ein Talent, Frauen auf eine solche Weise streicheln zu können, dass die Frauen den Fokus bei sich selber behalten, und die da drunter liegende Intention des Mannes nicht bemerken.
Das ist oft ein Spiel, das von den Männern gespielt wird, die das können.

Und leider, da ist ein kleines bisschen Anpassung darin.
Und das führt dazu, dass der Mann irgendwann, es satt hat, bei dieseR Frau zu sein.
Und sie versteht nicht warum.
Er sucht und findet eine andere.

Wir ertragen nicht die Anpassung, die man Rücksicht nennt.
Anpassung ist eine Falle, wenn sie nicht gebraucht wird.
Und wenn das eigene Leben nicht bedroht ist, dann ist sie nicht am Platz.
Die Anpassung ist schuld, an einem Großteil der 60% der Trennungen innerhalb einer Zeit von 10 Jahren.
Und so ist es in unserer Zivilisation.
Und diese Fahne ist so gewöhnlich, dass man es als normal ansieht, diese Forderungen an seinen Partner zu stellen.
Das es als normal angesehen wird, diese Forderungen an seinen Partner anzubringen.
Leider, so macht man nicht die Verbindung, dass dies der erste Schritt ist zur Trennung, diese Forderungen zu haben.
Und dass der Partner sich an sie anpasst.
Dann wird der oder diejenige,die etwas verlangt, zufrieden, und der andere die andere irgendwann uninteressiert.

Das Einzige, was haltbar ist, ist gerade zu verstehen, dass es nicht lebensbedrohlich ist für einen selbst, diese Probleme zu haben.
Und sie auf irgendeine andere Weise zu lösen, als dass der eigene Partner oder die eigene Partnerin sich anpassen sollen, damit diese Probleme nicht ausgelöst werden.
Aber du fühlst dich angeklagt von mir, dass ich auf der Seite des anderen bin.
So wie du meinst, dass ich gemein bin, wenn ich solche Sachen sage.
Und selbstverständlich darfst du darauf reagieren.
Du darfst darüber meinen und du darfst es ausdrücken.
Weil ich verstehe diese Enttäuschung.
Ich verstehe, dass du alles getan hast, was in deiner Macht steht um auszuhalten.
Und trotzdem funktioniert es nicht mit dem Mann, mit dem du wirklich dein ganzes Leben verbringen möchtest.
Frage mich weiter so können wir sehen, ob es möglich ist für dich, das auf eine andere Weise zu verstehen.

<div class="handwritten">
 Wenn dies das Sein ist, wie beschreibst du dann das Tun?
Oder meinst du, dass ist dies, was du vorher beschrieben hast, was ich meinem Mann erklären soll?
Das hat nämlich so geklungen, dass es an mich gerichtet war.
Aber er wird ja das, was du vorher gesagt hast, trotzdem nicht verstehen.
Das ist ja das Problem.
</div>

Es war an dich gerichtet.
Und es war nicht so gedacht, dass du es ihm erklären sollst.
Natürlich kannst du mit ihm reden über was auch immer du möchtest, worüber wir geredet haben.
Aber nicht weil ich es sage, sondern weil du das zu deinem gemacht hast.
Weil du das in dein eigenes umgewandelt hast und dann es sagst aus deinem Ausgangspunkt heraus, aus deiner Referenz.
Nicht dich dahinter verstecken, dass ich dir etwas sage, was du möchtest, dass er verändern soll, weil das geht nicht.
Zu deiner Frage zum Sein und zum Machen:
Sexualität ist ein Trieb und er tut sich selber.
Wenn du im Sein bist so treibt dieser das Machen zum sexuellen Akt.
Die erotischen Gefühle sind so stark, dass sie eigene Impulse haben.
Und auf diese Weise wird Sex zu einem Spielen, was man gemeinsam spielt.
Sobald du eine Bewertung hast, dass es auf eine gewisse Weise sein muss.
Und wenn das nicht so ist, dann bekommst du Angst und einen Krampf und es wird schmerzhaft.
Auf diese Weise bist du schon dort in dem sekundären Bewerten, was den Platz des Primären, des Seins übernommen hat.
Wenn du dann interpretierst, dass sein Wunsch nach Sex sekundär ist, dann wirst du selber zum Opfer, zu einem Objekt und fühlst dich gekränkt und vergewaltigt.
Aber wenn du verstehst, dass er im Sein ist und erotische Impulse bekommt, die den Beischlaf wollen, dann verstehst du vielleicht, dass er, da er dich liebt, im Sein ist und das tut, wozu ihm die Impulse kommen.
Ist das möglich zu verstehen?

Nicht, dass ich auf seine Seite bin, aber ich weiß, dass Männer oft auf eine anderen Weise funktionieren.
Sie bewerten nicht, sondern sind nur in ihrem Trieb und lässt diesen sie steuern.
Und auch diejenigen,die so sind wie du sie haben möchtest, sind im Grunde solche.
Aber sie lernen um,um das zu bekommen, was sie wollen.
Oft wird das ein Erfolg und du wirst entspannt.
Ist gleich: beide zufrieden.
Dein Mann spielt dieses Spiel nicht.
Er ist einfach und direkt und natürlich in seiner Sexualität.

Und dann bekommst du Angst, obwohl es vollkommen ungefährlich ist.

<div class="handwritten">
 Jetzt bist du wieder so kalt, sagst dass es mein Fehler ist und dass ich das auch ertragen sollte.
Pfui, das ist so unfair und gemein.
</div>

Ich sage nicht auf irgendeine Weise, dass er perfekt ist.
Er kann sicherlich auch seine Aufhängungen und haben und andere Verwicklungen haben.
Und es kann sein, dass du dabei recht hast, wenn es darum geht, dass er besetzt wird, Samenerguss zu haben.
Aber dein Problem ist, dass du dich dabei aufhängst und möchtest, dass Männer anders funktionieren als sie es tun.
Und es ist dieses, wo ich dir ein Zeichen geben möchte, es aufzugeben.
Gebe mir darauf keine Antwort, aber spüre nach, durchdenke dieses, lass es dich begleiten.
Und schau, ob dein Körper vielleicht sich damit versöhnt.
Dann wirst du den Kampf und den Schmerz in diesem Fall nicht mehr haben.

<div class="handwritten">
 Ja, ich weiß ja eigentlich alles, was du sagst.
Aber ich komme ja nie dorthin, weil seine Art mich zu berühren mir Angst macht, schon vom Anfang an.
Weil es fängt mit dem Machen schon vom allerersten Beginn an, sobald er geil ist.
Und da wird er sauer, weil ich ihm nicht entgegen komme.
Und dann willige ich ein, mich stattdessen vergewaltigen zu lassen, um seine Frustration nicht ertragen zu müssen.
Vielleicht kann ich das Anpassen sein lassen und stattdessen umdenken.
Vielleicht kann ich verstehen, dass es ungefährlich ist, und dann den Kampf sich auflösen lassen, oder ...
</div>

Ich meine, wenn du probierst und übst und nicht glaubst, was ich sage, sondern in der Wirklichkeit es praktiziert, dann bekommst du eigene Erfahrung davon, was dir hilft.
Und da bist du auf der Spur, mit der Zeit es sehr gut zu haben, auch wenn es um die Sexualität geht.
Das kann lange brauchen.
Da kann es viele abwegige Wege gehen.
Aber oft führt das zu einer tieferen Schicht der Bewertungen, davon, wie es zu sein hat.
Das es auf tausenderlei verschiedene Weise sein kann.
Und gut und schlecht sein auf genauso vielen Weisen.
Die Unvollständigkeit spielt mit uns, weißt du?

<div class="handwritten">
 Wie bleibe ich stehen.
Nein, wie bleibe ich in dem?
Das fühlt sich an wie eine Bedrohung in der Atmosphäre, die auch von ihm ausgeht, quasi, wenn ich nicht einwillige Sex zu haben, so geht er davon und hat Sex mit jemand anderem.
</div>

Das ist ein Hirngespinst, was du da hast.
Und du unterordnest dich, weil du daran glaubst.
Und das ist nicht wahr.
Er kann zu jemandem anderen gehen, egal wie gut oder schlecht ihr es habt im Bett.
Das ist nicht entscheidend.
Denn wenn er fremdgehen möchte, dann geht er fremd, egal wie ihr es habt.

Oft diejenigen, die zu anderen hingehen, haben es sehr gut zu Hause im Bett mit ihrem Lebenspartner.
Und deshalb bekommen sie Lust, mehr Sex zu haben, als der Partner es haben will.
Andre schieben die Schuld darauf, dass es ihnen zu Hause schlecht geht, damit sie eine Rechtfertigung haben zu anderen zu gehen.
Andere haben es gerne, viele viele Sexpartner zu haben und andere sind zufrieden mit einem.
Wir sind nicht monogam geschaffen, und daher sind alle Varianten möglich.

Mache es dir zurecht.
Schau, dass du damit fertig wirst.
Es ist eher weiblich als männlich, einen anderen Sexpartner aufzusuchen, wenn man spürt, dass der Mann vor der Intimität flüchtet.
Und dass sie irgendeine Form von Beiderseitigkeit im Sex haben will.
Da gehen Frauen oft fremd.
Und das handelt davon, dass sie sich anpassen an ihren Partner.
Und der Trotz ist genauso vorgegeben durch die Anpassung wie die passive Anpassung.
Es ist leicht, sich selber an der Nase herumzuführen.
Ich habe ja ein wenig dieses Hintergrundes bei der Einleitung erklärt.

<div class="handwritten">
Jetzt wirst du wieder so anklagerisch.
Ich fühle mich dann schlecht, weil ich solche Gehirngespinste habe.
Und du gibst uns Frauen stattdessen die Schuld.
Das ist so scheiß hart, dass du nicht verstehst, wie weh es mir tut.
</div>

Es tut mir leid, dass das, was ich sagte, deine Schuldgefühle ausgelöst hat und dein schlechtes Gewissen ausgelöst hat.
Das ist ganz wertlos und führt nur zu dem Opferpulli wieder, den du oft trägst, wenn es um Sexualität geht.
Ich kann nicht deine Gefühle erschaffen.
Ich kann nur etwas sagen, was Gefühle in dir hervorbringt.
Und wenn du diese Verteidigung fallen lässt, so nach einer Weile kannst du etwas anderes hören, in dem was ich sage.
Ich weiß, dass es kontroversiell ist.
Weil die ganze Zivilisation sich einig ist, dass die Frauen Opfer sind, wenn es zur Sexualität der Männer kommt.
Sogar soweit, dass es juristisch in Schweden nicht als Einwilligung gesehen wird, wenn die Frau nicht ja gesagt hat.
Das gibt Frauen eine für sie selber belastende Macht.
Das ist zugunsten von niemandem.
Weder von der Frau oder von dem Mann, eine Vormachtstellung in irgendeiner Richtung zu schaffen.
Wir bleiben an der Stelle stehen und dann kannst du mich wieder ansprechen und in „streiten“ oder weiterreden, so wie dein eigener weiterer Prozess ist.
