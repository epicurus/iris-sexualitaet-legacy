## Ich bin meistens zu müde, um Sex zu haben

<div class="handwritten">
Nächste Frage:
Es geht mir fast immer so, dass ich zu müde bin um Sex zu haben.
Und mein Mann sagt, ich sei wie ein toter Fisch, kalt und passiv und gleichgültig, lange Zeit, bevor ich mich erwärme.
Er findet, dass er alles getan hat, damit ich aufgelegt sein soll.
Er hat gekocht, er hat abgespült, er hat sich um die Kinder gekümmert unter anderem.
Und dennoch sage ich, dass ich zu müde bin, um mit ihm zu schlafen.
Er versteht, dass das nicht die Ursache ist.
Aber er fühlt sich so enttäuscht, dass er mich fragt:
 „Möchtest du mich nicht? Möchtest du dich stattdessen scheiden lassen, oder? Aber ich möchte das nicht.
Ich möchte nur, dass ich in einer Weise Ehe leben darf, aber er will das nicht.
Dann will er die Scheidung.
</div>

Du bedienst dich der Anpassung als ein Schutz davor, Sex haben zu müssen.
Du gehst hinein in einen Gefühlsspiel, wo du zum Opfer der Müdigkeit wirst und meinst es liegt an ihr, dass du nicht nicht Sex haben kannst.
So ist es nicht.
Es hat irgendwie damit zu tun, dass du dir nicht selber das Vergnügen gönnst, den Genuss gönnst, die Freude gönnst, Sex zu haben.
Vielleicht erlaubst du nicht, dass der Trieb sich einschaltet, dass du erregt wirst, dass du dich nach dem anderen sehnst, das Pirren im Körper aufkommen zu lassen.
Er tut alles, was ihm möglich ist, damit du das Licht herein kommen lassen sollst, und die Freude reinkommen lassen sollst in deinen Körper.
Aber es ist etwas da, was dagegen wirkt.
Ist das so?
Gebe mir keine Antwort darauf.
Aber sei ehrlich zu dir selber, diesbezüglich.
Ist es so, dass du den den Kick, die Süße spürst, die Macht zu haben, nein zu sagen, dadurch, dass du ein kalter Fisch bist.
Es kann daran liegen, dass man dich, so stark erzogen hat als du klein warst.
Dass es keine andere Möglichkeit gab, als dass du dich angepasst hast, als dass du ein braves Mädchen warst in den Augen deiner Eltern, damit sie sich als gute Eltern gefühlt haben.
Das kommt häufig vor und führt oft zu einer Form von Frigidität.
Es kann etwas gewesen sein, dass wie ein Schock war, wie ein Übergriff war der Sexualität eines Erwachsenen bevor du reif warst dazu.
Oder du kannst das Trauma von einem Menschen übernommen haben.
Und es ist wie deins geworden.
Suche in deiner Erinnerung und suche durch deine Sinne, dann wirst du herausfinden, ob du etwas findest.
