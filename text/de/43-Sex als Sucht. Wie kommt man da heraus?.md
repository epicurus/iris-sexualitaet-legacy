## Sex als Sucht. Wie kommt man da heraus?

Wenn man eine Neigung zur Sucht hat und einen ständigen Sog nach etwas von außen hat, dann ist es einfach, zum Sex-Süchtigen zu werden.
Dass man seinen Sog in die eigene Lust in den eigenen, einzigen Trieb, den wir als Menschen haben, in die Sexualität gießt, und dann wird man besetzt davon, auf dem Weg zum Sex zu sein, vom Sex weg unterwegs zu sein, oder besetzt von der Transportstrecke dazwischen.
Dann ist es nur das, was gilt und das Wichtigste darstellt.
Alles andere ist nur eine Pflicht dazwischen und muss gestehen, aber ganz ohne in sich selber zu sein und das Leben als den Lebenssinn zu erleben, und dass das Leben einen gegebenen Wert hat.
Wenn die Person dann eine Familie hat und Kinder hat, dann bleiben sie ohne Beziehung, ohne Beiderseitigkeit und tiefen Kontakt.
Weil die ganze Zeit ist etwa dazwischen.
Und daher ist der Kontakt nur ab und zu, einen Blick.
Wenn es einem selber zum Problem wird, ist das vielleicht Motiv genug zu verstehen, dass Hilfe gebraucht wird.
Man sucht vielleicht Hilfe für die eigene Sucht und zum einen dann psychologische Hilfe bekommt zum einen, zum anderen kann man ein Teil einer Hilfegruppe sein, anonymer Sexsüchtiger, wenn es das gibt in der eigenen Nähe.
Und man braucht hinzugehen so oft, dass man Zeiten hat, wo die Sucht nicht anspringt.
Und bis diese Zeiten länger und länger werden.
Man kann Jahre der Enthaltsamkeit brauchen und es kann sein, dass man uneinsetzender Süchtiger das ganze Leben bleibt.
Oder zeitenweise Süchtiger ist.

Dem wird auf eine Art von Wahn und Depression folgen.
Von der von dem einen Extrem ins andere.
Es ist das, was man heute bipolar nennt.
Leider bekommen viele Leute Medikamente, Psychopharmaka, und sie werden stattdessen apathisch, und das ist von der Asche in das Feuer.
Das, was die Person braucht, ist, eine unterstützende Person während vieler Jahre.
Wenn man einmal die Sucht angekurbelt hat, so ist es nicht einfach, sie auszulöschen.
Es ist eher so, dass man lernt damit umzugehen.
Wenn man sexualisiertem Benehmen ausgesetzt war, den Übergriffen der metoo-Bewegung, wenn man erwachsen war, durch jemanden, der eine Machtposition innehat, und man willigt ein, vielleicht um Vorteile zu erhalten, vielleicht um Nachteile zu vermeiden, vielleicht weil diese Kultur Norm geworden ist, dann wird das Problem, dass man sich selber als schuldig erlebt.
Man wird als verdreckt empfunden erlebt, exploitiert, prostituiert.
Und das macht Übergriff bei dem eigenen Selbstgefühl.
Und kränkt das eigene Vertrauen in einen selber und in die eigene Kapazität.
Das frisst sich tief hinein in das eigene Innere.
Und es sind öffentliche Geheimnisse, von denen die Umgebung oft weiß, aber alle schweigen stille.
Das zehrt an der Psyche auf eine Weise, die langfristig unerträglich ist, und was man entladen braucht, und eine Sprache bekommen braucht, so dass man Einhalt der ganzen endemischen Kultur bieten kann, die sich gerade um diese sexualisierten Verhaltensweisen herum entwickelt hat.
Die Individuen brauchen Hilfe und die Gesellschaft braucht dies zu internalisieren als ein destruktives Verhalten.
