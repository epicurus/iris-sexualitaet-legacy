## Kann man zuviel oder zuwenig Sex haben?

<div class="handwritten">
Nächste Frage:
Sex kann so genussvoll sein.
Ist es so, dass man zu viel oder zu wenig Sex haben kann?
Ich weiß ja, dass es Sexsucht gibt, aber dann ist es wohl oft pervers in irgendeiner Weise, und das meine ich jetzt mit diese Frage nicht.
Sondern zum Beispiel, wenn man jeden Abend Sex hat, ist das zuviel?
Ist das unnatürlich?
</div>

Nein, solange du nicht besetzt bist davon innen, sodass der Rest vom Leben lediglich eine Transportstrecke zwischen Sexgelegenheiten ist, sondern das anderes im Leben interessant ist und dann zusätzlich dazu hat man schönen Sex, dann ist es ganz natürlich.
Wenn es um zu wenig Sex geht, so gilt das gleiche.
Wenn du dich selber befriedigst oder Sex hast, wenn die Möglichkeit da ist und dazwischen nicht, und wenn du nicht eine Art von Perversität entwickelst, dann kannst du ohne Sex auskommen.

Die Gefühle kommen und gehen, aber sie brauchen keinen besonderen Auslauf.
Die sind eigentlich nur Lebenskraft und davon kann man ganz konstruktiv Gebrauch machen, und auf dienliche Weise für den Menschenkörper.
