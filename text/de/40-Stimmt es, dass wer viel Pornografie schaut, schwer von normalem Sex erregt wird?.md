## Stimmt es, dass wer viel Pornografie schaut, schwer von normalem Sex erregt wird?

<div class="handwritten">
Neue Frage:
Ich habe irgendwo gelesen, dass Männer, die viel Pornografie sehen, nur schwerlich von normalem Sex erregt werden.
Ist dies wahr?
</div>

Es kann so sein, dass wenn wir als Menschen uns dem aussetzen, etwas Exzeptionelles oft anzuschauen, wenn wir zum Beispiel Pornos angucken und das viel tun, das gilt nicht nur für Männer, sondern für uns alle, die wir Menschen sind, so wird ein Bild in uns eingekerbt, eine Vorstellung wird da eingekerbt, die zur Wirklichkeit wird, obwohl es nur auf dem Bild ist.
Wir werden von etwas geprägt, was bekannt und gewohnt und geborgen wird, und da glauben wir, dass dies wahr ist, leider.
Das kann Gewalttätigkeit sein.
Das können Wörter sein, Schimpfwörter, sexualisierte Sprache, Vorurteile, das können Gefühle sein, an die wir uns drankleben und von denen wir in Affekt geraten, und so weiter.
Das macht oft den Grund bei unseren Gedankenfehlern aus.
Und manchmal umfasst dies so viele Menschen, dass es zu einem Majoritätsmissverständnis wird.
Und da scheint es, wie wenn alle so sagen, und dann muss es ja wahr sein.
So ist es nicht.
Es wird nicht eher wahr, weil es viele sind, die daran glauben.
Das Problem wird nur größer.

Daher, es ist ein Sinn darin, keine großen Mengen von Süßigkeiten zu konsumieren, denn da wird man einen großen Sog nach Zucker bekommen.
Und da findet man nicht, dass es gut schmeckt, das, was nicht gesüßt ist durch den Zucker.
Das gilt auch für Pornografie.
