## Der Sexualtrieb ist gefährlich! Oder?

Der Trieb kann aus den Fugen geraten und da wird der sexuelle Impuls schädlich, zum Beispiel Inzest, Vergewaltigung, Gewalt in nahen Beziehungen, Totschlag, Eifersucht, gefühlsmäßige existenzielle Spiele, die in der Irrenanstalt, im Leichenschauhaus oder im Gefängnis enden.

Uns wird im Innersten bange, da wir nicht wissen wie unsere Reaktionen sein können, wenn wir in den Nebel des Unbewusstseins landen und dunkle Triebsimpulse sich einschalten.

Wenn wir mit dieser Ungeborgenheit, dass wir ein Trieb haben, geborgen sein können, aber dass uns möglich ist zu lernen, alle Handlungen, die der Trieb in Gang bringt, abzublasen,dass wir nicht ein Opfer vom Trieb sein müssen, dann ist er ein Ressource und eine Lebenskraft.

Aber, da er ein Trieb ist, springt er in vielen unpassenden Situationen an, und es ist dies, welches der Grund ist, dass wir so viel Angst vor dem Trieb haben. Denn wir haben Angst, dass er überhand nehmen wird, dass wir peinlich werden können, dass wir in den Augen anderer schlecht dastehen, dass wir vielleicht etwas nicht richtig deuten und es fällt auf... dass der Trieb uns zu etwas treiben kann, bei dem wir keine Kontrolle haben und zu dem wir nicht stehen können oder was nicht erlaubt ist... dass wir etwas tun könnten, weshalb wir uns schämen brauchen... es ist sogar auch beschämend zuzugeben, dass wir Menschen einen Trieb haben, der sich so oft einschaltet, wie er dies eben tut.

Wir wissen auch nicht, was wir mit den Reaktionen bei uns selber tun sollen, wenn wir mit dem Trieb von anderen in Berührung kommen... dass wir da in dem Schutz-und Verteidigungssystem des Autopiloten landen, wir bleiben erstarrt... starr im Denken, im Fühlen und im Handeln, nichts ist möglich, wir werden paralysiert, und kommen in ein Außer-uns-selber-System hinein, was zur Folge hat, dass der andere „was auch immer“ mit unserem Körper tun kann.

Viele von denen, die Opfer vom Inzest, von Vergewaltigung werden, Leute, die geschlagen werden usw, zeugen davon. Daher brauchen wir die andere Seite des Autopiloten, programmieren zu lernen... dass wenn uns etwas Unheimliches angetan wird, was von der Sexualität des anderen herkommt, dann schreien wir, kratzen, beißen, treten, befreien uns und rennen davon.
Dann gelangt dieser Antagonist in eine Paralysierung und dann ist die Gefahr vorbei.

Früher wurde dies kleinen Kindern beigebracht. Aber diese Initiation ist ausgefallen und braucht eine Wiederbelegung, wenn wir auf den Ausmaß der "Me-too"- Bewegung schauen.

So gewohnt und vertraut mit dem eigenen Trieb zu werden, dass er kommen und gehen mag, wie ihm nur beliebt, ohne auf das Machen von ihm fixiert zu sein... und dass wir erleichtert, Puhh.. auf ihn hin nichts unternehmen brauchen... zu diesem fehlen vielen die Aufklärung und sie bekommen deshalb so viel Angst vor diesem Trieb... vor dem mit-sich-selber-nicht-werden-umgehen-können bei gewissen Situationen.

Leider üben wir uns selten darin, mit den natürlichen Schwierigkeiten im Leben umzugehen - wir möchten sie nur aus dem Weg haben, sie sollen nicht da sein. Das mag ja bei vielem der Fall sein, ist es aber nicht bei Trieben und Sucht.

