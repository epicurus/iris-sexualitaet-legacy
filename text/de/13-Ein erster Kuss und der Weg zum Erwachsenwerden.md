## Ein erster Kuss und der Weg zum Erwachsenwerden

<div class="handwritten">
Dann weiß ich, dass als ich in der vierten Klasse war, so gab es einen Jungen der zwei Jahre älter war als ich.
Er zog mich hinter eine Hecke hinein, die auf einem Grundstück neben der Schule stand, und er umarmte mich fest und küsste mich auf den Mund.
Es war vielleicht nur eine Minute und dann rannte er weg.
Ich blieb stehen und kam in einen euphorischen Zustand, in einen Seligkeitsrausch.
Es entstanden ein Sog und ein Pirren im ganzen Körper und ich wusste nicht, was es war, oder was ich damit tun sollte.

Nach einer Weile war das Schlimmste vorüber, aber es kam jedes Mal wenn ich mich an diesen Kuss auf meinem Mund erinnerte zurück.
Ich habe vermutet, ich bin verliebt, und ich fing an, mich für Mädchenliteratur, für Bücher, Zeitungen und Fernsehsendungen, die Liebe zum Thema hatten, zu interessieren.

Davor hatte ich nicht an meine Mutter und an meinen Vater als einzelne Menschen gedacht, als einzigartige und geschlechtsmäßige Geschöpfe, die eine Beziehung hatten.
Das ging mir auf und ich fing an, darauf zu schauen, wie sie sich gegenüber einander verhielten.
Früher war das nur eine Einheit gewesen, die zusammengehalten und die sich um mich und meine Geschwister gekümmert hat... die darauf schaute, dass alles weiter ging und sie mischten sich sofort ein, wenn man etwas Dummes tat.


Meine Mutter nutzte die Gelegenheit und erzählte von dem, was zur Frau gehört, dass ich wohl bald meine Periode bekommen würde und wie ich damit umgehen sollte.
Dass, wenn dies geschah, würden die Jungs auf eine andere Weise als jetzt, Interesse an mir bekommen, und sie würden Sex haben wollen mit mir und dass es mir nach Gesetz erst mit 15 Jahren erlaubt war.
Sie erzählte, dass man schwanger werden konnte und was das auf sich hatte, und dass man mit dem Miteinander-Schlafen warten sollte bis einem sicher war, dies sei ein Junge, dem "ich mich selber wirklich hingeben möchte"... wie sie es sagte.

Da hörte meine Kindheit auf.
Sie wurde sehr radikal zur Geschichte und im Leben fing es an, ums Attrahiertsein zu gehen, was in mir durch manche Jungs hervorgerufen wurde.


Die Teenager Zeit fing an, als ich etwa 13 Jahre alt war und sie dauerte an, bis ich meinen Mann kennenlernte... damals war ich 18 Jahre alt, hatte gerade mit der Hochschule begonnen.
Wir wurden uns einig, unsere Ausbildungen erst abzuschließen und bis dahin an den Wochenenden und in den Ferien Zeit miteinander zu verbringen, und ab und zu uns über eine Tasse Kaffee in irgendeiner Kneipe zu verabreden.
</div>

Okay, was hast Du an Problemen in deinem Teenager- und Erwachsenleben erlebt, wenn es um die Sexualität geht?

<div class="handwritten">
 Es gibt bei mir in vielen Richtungen eine ganze Menge an Fragezeichen, und diese werde ich dir vorlegen und dich um eine Antwort darauf bitten.
Wir haben eine Gruppe, wo wir über Beziehungen und die Schwierigkeiten in unseren Beziehungen diskutieren und bei dieser werden viele Sexprobleme angesprochen, die ich dir vorstellen werde.

Mir ist klar geworden, dass du eine ganze Menge interessante Sachen zu nahen Beziehungen zu sagen hast, und es sind ja in diesen nahen Beziehungen, wo die Probleme oft auftreten.
Sowohl zwischen Eltern und Kindern, aber auch wenn die Kinder junge Erwachsene werden, da werden die Eltern oft neidisch und verletzt, weil sie nicht meinen, dass der Partner oder die Partnerin von den Kindern gut genug ist.
So war es bei mir.

Meine Eltern stellten meinen Freund und seine Gewohnheiten auf eine unfaire Weise in Frage, was mich sehr verletzt hat.
Eigentlich tat mein Vater das nicht, aber er hat immer gemeinsame Sache mit meiner Mutter gemacht und das tat er jetzt auch.
Ich zog ihm deshalb vom Leder, und danach wurde er sehr introvert, hat kaum mit mir oder mit meiner Mutter geredet...

Ja, und dann meinte sie, dass es viel zu früh ist, dass wir uns verloben.
Ihrer Meinung nach waren wir immer noch bloß Kindlein.
Das hat mich zutiefst verletzt und ich zog bald in ein Studentenwohnheim und meiner ganzen Studienzeit wohnte ich dort, obwohl ich an der Uni meiner Heimatstadt war.
Es ist schade, dass die Eltern so doof sind, dass sie der Anlass werden, dass ihre jungen  erwachsenen Kinder von ihnen Abstand nehmen.
</div>

