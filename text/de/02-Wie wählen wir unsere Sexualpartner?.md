## Wie wählen wir unsere Sexualpartner?

<div class="handwritten">
So war das also, das gibt auch was zu Denken. Aber in mir ist es so unvorstellbar, mir einen Partner zu denken, bei dem ich schon von Anfang an keine erotische Anziehung spüre... 
das scheint unfair, in dem nicht zu sein, mit jemandem zu sein, der nicht dieses Pirren und diesen Sog in mir aufweckt.
Jemanden aus einem anderen Grund zu wählen, kommt mir falsch und Lügenhaft vor,
wie wenn ich da jemanden anschmiere, eine Beziehung mit mir anzufangen.
Ich bin damit indoktriniert, wie könnte ich es dann anders?
Ich weiß, dass viele meiner Freunde auch so denken und deshalb möchte ich das gerne klären.
</div>

Der Grund, weshalb wir heute so denken, ist dass wir nicht verstehen, dass die Sexualität ein Trieb ist, und dass er immer aufwachen kann, wenn wir einander nahe kommen, wenn wir die Worte und Auffassungen des anderen schätzen, wenn wir uns unseren Sinnen bedienen, um in der inneren Zufriedenheit, die uns eigen ist, zu sein. Und dann gibt es nichts Besseres, als jemanden zu haben, mit dem man diese teilen kann, und dann, ja dann werden wir empfänglich und sensitiv und in das gießt sich die Sexualität und wir fühlen uns erotisch angezogen.

Es kann die Art sein, wie ein anderer Mensch lächelt, wie er riecht, die Anmut und Melodie der Stimmlage oder die tänzerische Bewegung, wenn ein Mensch läuft, sein, und so weiter.

Es setzt voraus, dass wir uns nicht aneinander anpassen, dass es an uns liegt, die Bedürfnisse des anderen zu befriedigen, sondern dass wir an stelle dessen das Bedürfnis vom anderen zu dem werden lassen, dass wir ein Geschenk bekommen, mit dem wir zufrieden sind und dann können wir so tun wie der andere es möchte, ohne es ihm nur recht zu machen. Und dann kommt der Widerstand in uns selber gegenüber der Sexualität in der Beziehung nicht auf.

Wir meinen oft, aus dem Vorurteil, dass vereinbarte Ehen, nicht glücklich sein konnten, denn sie fingen bei der Anziehung nicht an. Aber diese Ehen bauten, ganz klar darauf, dass die Familien einander kannten, und es war wohl stimmig jemanden zu heiraten, der den gleichen Hintergrund hatte, und da konnten sie es gut miteinander.
Wenn es zu den jungen Menschen kam... die Erwachsenen wussten oft aus eigener Erfahrung, dass wenn diese nur sich sehen und im Beisein von anderen Zeit miteinander verbringen würden, würden die Anziehung und das Verlangen sich einstellen, da würden sie sich nach Sex sehnen, und da würden sie heiraten wollen, weil ihnen vor der Ehe nicht erlaubt war, miteinander Sex zu haben.
Und in der Tat... Es entstanden hierdurch genau so viele glückliche und wohl funktionierende Beziehungen wie auf die Art, wie wir es jetzt machen, indem wir eine Beziehung auf etwas so Flüchtiges, wie auf die sexuelle Attraktion bauen. Ich befürworte nicht das eine oder das andere, ich mache nur deutlich wie der Trieb funktioniert.

Wir sind alle Menschen und wir haben alle diesen Trieb, aber wie wir dem gemäß funktionieren ist eins der wenigen Dinge im Leben, wo wir als einzigartige Menschen uns von einander unterscheiden. 
Ich bin nicht sicher ob dies von der Konstitution herrührt. Es kann genauso gut an der Umwelt und an traditionellen Strukturen liegen, die über die Generationen hin angelernt werden.
Ich werde hier einige von ihnen nennen, um auf Ähnlichkeiten und Unterschiede zu schauen.
Und trotzdem ist es der Trieb, der auf eine ausgeklügelte Weise hilft, Beziehungen zum Kindergebären zu bringen.