## Gefühle werden nicht durch den anderen erzeugt, auch Eifersucht nicht

<div class="handwritten">
Was meinst du, dass es ist, was das auslöst, was du Gemeinheit nennst oder Sadismus nennst?
Oder dass jemand einem anderen Menschen etwas antut,oder ihm verfolgt bis der oder diejenige zusammenbricht und vielleicht aufhört mit dem Leben, vielleicht.
</div>

Dadurch, dass die Sexualität ein Trieb ist und nicht in Zusammenhang steht mit Bewertungen, so wird sie aus dem Lust-Unlust Prinzip heraus bestimmt.
Was von dem reptilienhirn ausgeht, und was anspringt in allen möglichen, geeigneten und ungeeigneten Situationen.
Und die Sexualität ruft sehr tiefe und ursprüngliche Reaktionen in uns hervor, zum Beispiel Eifersucht.
Es wird wie bei den eigenen Kindern, wenn etwas sie von außen her bedroht, was dazu führen kann, dass ich als Elternteil mein Kind verliere.
Dann kommen starke, aggressive Verteidigungs- und Schutzinstinkte hervor, die zu unsozialen Handlungen führen.
Das heißt, den oder diejenige zunichte zu machen, der oder die einen solchen Angriff vornimmt an einem Menschen meine Herde.

Oft sind es Überbleibsel aus der Kindheit der Menschheit als es um Leben und Tod ging.
Und weshalb dann das Prinzip waltete, Auge um Auge, Zahn um Zahn.
Dies konnte ganze Geschlechter vernichten, dadurch dass jemand angefangen hat.
Und es ging in Generationen so weiter.
Oft wurde Frieden gestiftet dadurch, dass jemand seine Tochter zu einer anderen Familie hingab oder umgekehrt.
Und durch die Ehe hörte das Töten auf.

Bei den modernen Eifersucht geht es um Monogamie.
Dass wir dabei die Sexualität eines anderen uns selber vorbehalten und da haben wir Vorstellungen was es heißt:

Du darfst keine andere Sexualität neben mir haben.
Nicht einmal Lust oder Gefallen an einem anderen.
Denn dann werde ich verletzt und gekränkt und ich fühle wie wenn du auf mich trittst.

Das Schwierige daran ist, dass wir mehr und mehr hinzufügen, was wir als Untreue deuten könnten gegenüber der Monogamie.
Wir können es darauf anlegen, solche Gekränkheitsgefühle zu sammeln, bis wir finden, dass wir sie eintauschen sollen, indem wir die Beziehung beenden.
Dass der andere unwissend bestraft wird wegen etwas, von dem der andere überhaupt kein Teil ist.
Was nur vor sich geht in dem Gefühlsleben eines Menschen.
Dieser Mensch wird bestraft, indem er verlassen wird durch seinen Partner.
Ich nenne das den Konflikt mit Teilung zu lösen und dadurch die Beziehung beenden, und zur nächsten zu gehen und das gleiche Muster wiederholen, anstatt die eigenen Konflikte zu lösen.

Konflikte werden erzeugt und sind in einem selber und haben nichts mit dem anderen zu tun.
Auch wenn wir oft sagen: Du machst mich traurig.
Aber es geht nicht.
Der andere kann die eigenen Gefühle nicht schaffen, sondern „du tust etwas, weshalb ich traurig werde“, „du tust etwas weshalb ich Freude habe“, innen ist es ein meilenweiter Unterschied.

Wir glauben, dass es etwas Allgemeines ist, was verletzt.
Aber das ist es nicht.
Das ist höchst spezifisch und kommt daher wie unsere eigenen Ängstesysteme aussehen und wovon wir geprägt worden sind davon, wie unsere Eltern einander behandelt haben.
Das sind die stärksten Strukturen, aus denen her wir bestimmt werden und über die wir geprägt worden sind.

Während es sich um dieses dreht, so ist es oft möglich, durch Reden darüber damit zurechtzukommen.
Jeder Einzelne kann Hilfe empfangen und dort hinkommen zu verstehen, dass Liebe kein Produkt ist, sondern ein Zustand, und dass kein Mangel an Liebe ist.
Nur Mangel an der Fähigkeit, diese zum Gefühle heranzuziehen.
Dass die Liebe angebaut werden braucht und herausgeholt werden braucht durch uns selber.
Und hinunter verschmolzen werden braucht in die Zufriedenheit, innen, so dass wir es hinkriegen, gemütlich gegenüber unseren Partner zu sein, so dass der andere sich wohl fühlt, guten Gemüts ist, offen verletzlich und neugierig ist auf uns selber.
Und dass das auch zwischen uns herrscht.
Dass Ängste der Anfang einer Teilung sind, wenn wir nicht sofort darauf setzen, aus dem Gespenst herauszukommen, anstatt, dass wir den anderen begrenzen wollen.

Was wir stattdessen brauchen ist einzusehen, dass der oder die andere mich gewählt hat.
Und gerade mit mir zusammen ist und das heißt, dass er oder sie mich will, gerade mich, und dann vermutlich sehr vertrauenswürdig ist.
Jedenfalls ausreichend, bis der Gegensatz sich gezeigt hat.
Und wir hoffen ja, dass er sich nicht zeigt.

Die Kunst besteht also darin, nicht den Gegensatz zum Vorschein zu treiben, denn das Risiko ist da, dass es uns gelingt.
Und dass wir es uns selber kaputt machen in der Beziehung.
