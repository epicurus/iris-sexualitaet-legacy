<div class="impressum">
    <strong>Bitbucket</strong>: <a href="${BOOK_REPOSITORY}">michaelschmidt_berlin/2064</a><br />
    (Beiträge erwünscht ...)<br />
    <strong>ASIN</strong>: ${BOOK_ASIN}<br />
    <strong>ISBN</strong>: ${BOOK_ISBN}<br />
    <strong>Version</strong>: ${BOOK_VERSION}.${BITBUCKET_BUILD_NUMBER}.${BITBUCKET_COMMIT}<br />
    <strong>Lizenz</strong>: ${BOOK_LICENSE}<br />
    <strong>Mitwirkende</strong>:<br />
    Volker Diels-Grabsch<br />
    Maarja Urb<br />
    Benedicte Walentin Moe<br />
    Christian 'Crille' Vandrei<br />
    Rainer-Maria Fritsch<br />
</div>

<div class="title-page">Für Julian Assange</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="title-large">Teil 1</div>
<div style="visibility: hidden;">\pagebreak</div>
