## Mein Mann hat nie den Impuls Sex zu haben. Ich mache immer den ersten Schritt

<div class="handwritten">
Nächste Frage:
Ich habe einen Mann, bei dem es nicht sehr aufkommt, dass er Sex haben will.
Und ich bin eine solche, die das möchte.
Ich habe es gern.
Ich habe es gern, in dieser Nähe sein zu dürfen und diese Entspannung zu bekommen.
Und diese Begegnung mit meinem Mann zu haben.
Aber immer immer bin ich, der den ersten Schritt unternehme.
Oft sagt er dann: Können wir das nicht heute Abend lassen oder wenn es sein muss dann, okay.

Sein Glied wird steif und er kann es mir angenehm machen, wann ich auch es will.
Aber ich vermisse seine Initiative.
Gibt es etwas, was ihn dazu bringen kann, selber gerne Sex zu haben.
</div>

Er befindet sich in der Situation wie es viele Frauen tun.
Sie haben eigentlich nichts gegen Sex, aber sie haben keinen eigenen Wunsch danach.
Sie warten auf die Initiative des anderen und geben sich damit zufrieden.
Manchmal liegt das daran, dass wir einen Instinkt haben, dass wenn sowieso keine Kinder dabei rauskommen dann kann es bis später warten.
Während für andere ist es so viel Vergnügen und so viel Genuss, das Loswerden von Ängsten, und das Loswerden des Irrgefühls.
Das ist dann der Grund, dass die Initiative sich einstellt.

Das Wichtige ist, dass du nicht ein Bild hast, dass es auf eine gewisse Weise sein soll.
Sondern das es verschieden bei verschiedenen Menschen ist.
Und das ist natürlich.
Auch wenn es manchmal ungewöhnlich und es deshalb nicht normal (= allgemein vorherrschend).
Das ist natürlich.

So ist mein Rat, das zu genießen, wenn du es möchtest, dass du die Initiative hast, und dass dein Mann zu dir kommt und dass ihr es gut habt.
Lasse alle Konventionen fahren und lebe die Aufgabe im Verantwortlichsein in dir.
Dann wirst du zufrieden bevor und während der Zeit.
Und du wirst zufrieden sein danach.
Geniiiieße es.
