## Eifersucht ist kein Zeichen für Liebe

Ein Gedankenfehler, der sehr häufig vorkommt, ist dass man glaubt, dass Eifersucht ein Beweis der Liebe ist.
So ist es nicht.
Es ist nur ein Ängstesystem,das Interesseund das Zusammensein eines anderen zu verlieren, was die Eifersucht auslöst.
Die Kunst ist also, nicht darauf herumzureiten, mit anderen zu flirten, für Andere zugänglich zu sein bis der eigene Partner oder die eigene Partnerin schwarzen Sinnes wird.
Und selber einen Kick zu bekommen durch die Eifersucht des anderen.
Sich dadurch geliebt zu fühlen.
Dies hat keine Wirklichkeitverankerung.
Es ist nur ein Gefühlsspiel.

<div class="handwritten">
Aber sind es meistens die Männer die eifersüchtig sind? Und wenn das so ist, warum ist das so?
</div>

Nein, es hat mit dem Geschlecht nichts zu tun.
Es hat viel zu tun mit einem gefühlsmäßigen Spiel.
Wir möchten so gerne, dass der andere oder die andere uns bestätigt von außen, damit wir dadurch einen Wert bekommen.
Dass der oder diejenige uns liebt, sodass wir dahin Geborgenheit finden können.
Diese Geborgenheit ist nur ein Schein.
Es ist in etwa das Gleiche wie wenn Menschen sich gerade kennengelernt haben.
Dann dann verbringen Sie nicht nur Zeit miteinander, sondern sie schlafen miteinander.
Sie gehen zu Bett, treiben Geschlechtsverkehr, um ein Beweis zu haben, dass sie ein Paar sind.

Das ist auch ein Grund des Scheines.
Es beweist nichts,nur dass sie ganz natürliche Geschöpfe sind, die aus ihrer Fortpflanzungslust heraus gesteuert werden.
Das hindert niemanden daran, andere Sexpartner gleichzeitig zu haben.
Und es ruft eher die Lust für andre hervor anstatt, dass es jemanden sättigt, nicht Interesse daran zu haben.
Außerdem ist es so, dass wenn du Sex hast mit jemandem, so riecht das.
So ist der Geruch um dich und das erweckt die Lust bei fremden Menschen, die dann vielleicht den Versuch machen, das Gleiche zu bekommen.
Und die leicht schwarzen Sinnes werden an der Lustkrankheit.

Und wollen sich rächen bei dir, der oder die du diese Lust hervorgebracht hast, und dann die Personen abgewiesen hast.
Männer verfahren auf eine Weise mit diesem, und das kann manchmal zu Gewalt führen.
Während die Frauen ganz anders damit vorgehen.
Sie sind wie (Hundrenn ?), die den Mann in einen Gefühlsmorast hineinlocken.

<div class="handwritten">
Meinst du das Ernst?
Aber das ist ja das Gegenteil von dem, was die meisten glauben.
</div>

