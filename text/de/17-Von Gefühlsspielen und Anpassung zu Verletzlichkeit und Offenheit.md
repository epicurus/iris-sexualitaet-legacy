## Von Gefühlsspielen und Anpassung zu Verletzlichkeit und Offenheit

Die meisten Menschen, die ich kenne, die sagen, sie machen bei dem Sex mit, eher als sie es selber möchten, damit der Mann das Interesse nicht verlieren soll an ihnen.
Und dann bekommen sie einen Widerstand, gerade weil sie sich dazu gezwungen fühlen da mitzumachen, gerade weil sie ein Interesse haben an diesem Mann.
Sie denken gar nicht, dass dies Erwartungen beim Mann hervorruft.
Weil sie am Anfang da eingewilligt haben.
Und sie wünschen, dass er sie respektieren soll, dadurch dass er nicht mit ihnen schlafen will, sondern Interesse hat an ihnen als einzigartigen Individuen an erster Stelle.
Kann es so ein Fehldenken sein?

<div class="handwritten">
Ja, ich weiß.
</div>

Und deshalb ist es wichtig, das einfach enthüllt zu bekommen, so dass wir nicht die Möglichkeiten kaputt machen, nebeneinander her zu leben und einander kennenzulernen, bevor der Trieb überhand nimmt.
Früher oder später, während gewissen Zeiten, tut er das.
Aber dass er warten kann bis wir wirklich einander kennengelernt haben und einander in der Begegnung wollen, in der Beiderseitigkeit einander wollen.
Was beide in der Richtung hin verändern, zu einem Wir hin.
Mädchen hätten es nötig, das mit dem Trieb zu lernen, dass er anspringt und dass die Männer deshalb es versuchen bei derjenigen, die den Trieb hervorruft.
Und dass dies nichts mit dem einzigartigen Individuum zu tun hat.
Es hat nur damit zu tun, dass jemand die Lust zum Sex bei dem anderen hervorruft.
Sie hätten es nötig zu lernen, dass sie den Partner genauso oft gewinnen wie verlieren, wenn sie Sex haben, und das es ihnen viel besser gehen würde, wenn sie dann an der Stelle einfach das Spiel spielen würden, das tierische Verhalten zu brechen.

Auf Westgötisch, das ist ein Dialekt in Schweden, gibt es einen besonderen Spruch.
Und jeder weiß was es heißt:

„Halte die Fäustlinge bis wir einander kennen“.
Das klingt viel voller im Dialekt.
Aber im Dialekt verstehen nur diejenigen das, die es eben kennen.
Gerade so einen Spruch zu haben, den man sagen kann und den alle kennen, und dessen Bedeutung alle kennen, das schafft den Raum miteinander nebeneinander her zu leben bis niemand sich anpasst.

<div class="handwritten">
Wie ist das dann bei den Jungs?
</div>

Sie fühlen sich oft gezwungen, es bei einem Mädchen zu versuchen.
Weil sie meinen, dass sonst wird das Mädchen sie verlassen, zugunsten eines selbstsichereren Jungen.
Und das führt bei ihnen auch zu einer Verachtung, weil es viel zu leicht ging, sie zu verführen.
Und da erleben die Jungs dieses Mädchen als nicht vertrauenswürdig.
„Jeder kann sie ja verführen, wenn es mir so leicht gelingt“.
Diese Enttäuschung führt oft zum Prahlen bei den Jungs innerhalb der Kumpelschaft.
Und diese Geschichten prägen auf die Art, wie man auf diese einzelne dieses einzelne Mädchen schaut.
Es wird zu einem Konkurrenzspiel unter den Jungs.
Wer Don Juan am meisten ähnelt.
Die Mädchen verzaubern und bezaubern.
Und die Jungs verführen.
Der Unterschied liegt daran, dass die Mädchen sich hübsch machen, um anderen Menschen vorzugehen bei der Konkurrenz.
Und die Jungs sind cool oder hart, um Mädchen zu bekommen.
Diese Gefühlspiele sind im Grunde dazu da, sich selber daran zu hindern, offen, verletzlich und stark zu sein.
Eins mit sich selber zu sein, sich selber Hier und Jetzt zu sein.
Anwesend in Begegnung.

Diese Intimität ist ungewohnt und ruft oft Schmerz und Unbehagen hervor, weil man glaubt, dass man schwach ist, wenn man offen und verletzlich ist.
Und es ist das Umgekehrte: Da ist man am stärksten und verträgt das meiste und verträgt am besten die Unvollständigkeiten des Lebens.
