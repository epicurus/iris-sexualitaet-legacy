## Sexualität ist nur ein Trieb

Du hast mich gebeten, dir und auch deiner Freundin zu erklären, die heute mit dir gekommen ist, warum ich sage, dass Erotik und Sexualität nicht Liebe sind, sondern ein Trieb sind, ein Werkzeug zum Überleben der Menschheit und gerade ein Trieb _sind_, der sich einschaltet, wenn wir jemanden begegnen, der oder die ein potentieller Partner oder eine potentielle Partnerin sein kann.
Und dass die Erotik in allen möglichen und unmöglichen Situationen sich einstellen kann.
Und ich sage, dass die Natur trotzdem ziemlich lieb ist und die Partner ziemlich in Ordnung aussucht, die auch zu uns passen können nachdem die Verliebtheit vorbei gegangen ist und nachdem die Kinder von zu Hause aus dem Haus sind.
Dass Lust ein etwas instabiler Grund ist, eine lebenslängliche Beziehung aufzubauen, weil sie hinterhältig kommen und gehen kann, ein bisschen wie es ihr beliebt, und mit uns ihr Spiel treiben kann wie wie tanzende Reflektionen des Sonnenlichtes, die an eine Wand Spiegel und uns manchmal blenden.

<div class="handwritten">
Du bist so krass und hart wenn du so sagst.
Wir andere, wir wollen etwas Romantik haben, und wenn du das so sagst dann scheint es so als wäre sie nur ein tierischer Trieb und ich finde nicht, dass das stimmt.
</div>

Du hast recht, sie kann viel mehr als das sein.
Und sie kann alle anderen Gefühle, die du fühlst auch beinhalten und auch die, die für dich zentral sind.
Aber warum ich dies so sage ist, weil die meisten überspringen, sich damit auseinanderzusetzen, dass es in der Tat ein Trieb ist, der im Dienst der Menschheit steht, zum Überleben der Art.
Viele erheben sie zu Geistigem, zu Heiligem, und wollen das zu etwas Göttlichem machen, und das ist sie nicht.
Schon, wir können irgendetwas erhöhen, hinauf in eine Metaebene, die man nie beweisen kann.
Erst wenn wir gestorben sind bekommen wir die Antwort, ob wir zu Gott gelangen oder nicht, wenn es einen Gott gibt, zu dem man kommen kann.

Aber wir haben alle jedes Recht an Gott zu glauben und ihn für wahr zu halten und dass _er_ heilig ist.
Der Schmolch im Becher ist, wenn du in Wirklichkeit das Vergnügen, den Genuss, wonnigliche Seligkeit und so weiter in Beziehung zu dem, mit dem du die Erotik hast, erlebst, dann, nach heiligen Büchern, die von Menschen geschrieben sind, enttäuscht du Gott.
Es ist nur in Beziehung zu Gott, wo dir erlaubt ist, das große Glück zu erleben, Agape.
Mit einem anderen Menschen darfst du nur das niedrigere, Eros erleben und nur wenn du vorhast, Kinder zu zeugen, ansonsten gilt Enthaltsamkeit.
Denn unterbrochener Geschlechtsverkehr ist Selbstbefleckung und eine Todesünde.

Und es ist ja auch so, dass wenn wir in der Nähe eines Menschen leben, so gibt es da eine Überschneidung, die Atmosphäre des einen färbt die Atmosphäre des anderen ein und auch anders herum. Und deswegen fangen wir an, Eigenschaften vom anderen zu schätzen, mit denen wir vorher nicht vertraut gewesen sind. Das führt manchmal zu einem Zusammenklingen und dazu, dass wir in dieser Berührung durch den anderen Liebe zum Gefühl in uns heraufziehen und das kann über die Jahre hin wachsen.
Früher, als die Leute sich örtlich nicht groß bewegt haben, fanden sie ihren Partner oft  innerhalb eines Radius von 4 km, und oft waren sie auf der selben Schule, kannten ihre Familien gegenseitig und hatten die gleichen Traditionen, und da geschah das Zusammenklingen schon von Anfang an.

Natürlich kann es auch in die andere Richtung gehen. Das was man am Anfang so bezaubernd fand, wird nach einer Weile als äußerst unangenehm und störend empfunden und schafft Abstand, anstatt dass wir uns einander annähern. Und da wird es oft so erlebt wie wenn die Liebe ausgeht, was sie nicht tun kann, aber man hört auf, sie heraufzuholen, sie in einem selber heranzuziehen, und kultiviert stattdessen negative Bewertungen.

