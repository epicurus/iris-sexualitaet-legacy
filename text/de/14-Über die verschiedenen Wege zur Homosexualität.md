## Über die verschiedenen Wege zur Homosexualität

Ich habe mir viel überlegt, warum manche Menschen homosexuell werden, und warum es so wichtig für sie ist, sich zu outen, wie sie sagen.
Kann es daran liegen, dass sie einen Konflikt mit ihren Vorbildern, ihren Eltern haben?
</div>

Es können mehrere Gründe sein.
Ein Grund ist biologisch und hat mit dem Genetischen und mit den Hormonen zu tun, dass es aus irgendeinem Grund so ist, dass man nicht angezogen wird, nicht Lust zum anderen Geschlecht bekommt, aber auf jemanden vom eigenen Geschlecht Lust bekommt.
Dies kann man nicht überwinden, auch wenn wenn man lange Zeit meinte, dass man das wohl einfach überfahren sich eine Frau oder einen Mann finden kann.
So ist es nicht.
Früer oder später entsteht ein existentieller Konflikt innen.
Dem folgt eine Depression.
Und da bleibt oft nicht die Beziehung, die man hat, bestehen.
Wenn der Partner oder die Partnerin keine nahe Beziehung zwischen dem Partner und seinem Partner gleichen Geschlechts erlaubt.

Manchmal kann es ohne biologischen Grund sein. 
Es kann so sein dass jemand einem über den eigenen Weg lief, der einfach ein herrlicher Mensch war, der genau gepasst hat, und dieser Mensch hätte jegliches Geschlecht haben können, aber er wurde zu dem einzigen, mit dem man das Leben teilen will. 
Manchmal liegt es daran, dass jemand in einer Symbiose mit dem anderen Geschlecht feststeckt, und da ist es nicht erlaubt, sexuelle Gefühle für dieses Geschlecht zu haben.
Es ist besetzt von etwas anderem, was dem im Weg steht, und deshalb löst die Person an der Stelle dies mit gleichgeschlechtlichen Beziehungen.

<div class="handwritten">
 Warum ist die Homosexualität so lange Zeit verboten gewesen und bestraft worden?
</div>

Das hat mit den Gesetzen zu tun, die auf das Verurteilen durch die Bibel und auf die Untreue gegenüber dem einen allmächtigen Gott bauen, zu tun.
Dies, genauso wie die Selbstbefriedigung... Männer dürften nicht mit den Händen unterhalb der Decke schlafen... die Männer dürften nicht mit Tieren schlafen.
Es war bis 1944 mit Todesstrafe belegt, wurde dann abgeschafft.
Dann wurde es erlaubt, 2014 wurde wieder ein Gesetz erlassen, weil man es als Tierquälerei ansah. 
Die Homosexualisierung wurde 1944 entkriminalisiert, aber sie galt bis 1979 als psychische Störung, und immer noch wird es von vielen als pervers angesehen.

Immer mehr sind es menschliche Codes und Normen geworden, die den Trieb, die Sexualität regeln.
Das Einverständnis ist in Schweden das Wichtige geworden, und daraus folgt, dass niemand den anderen in sexuellen Spielen schadet.
Erst wenn einem Menschen Schaden zugefügt wird, wird die Handlung, die zum Schaden geführt hat, hinterfragt, aber dabei im Zusammenhang mit dem was diese Gemeinheit oder diese Bösartigkeit bei dem Täter ausgelöst hat.
Schädliche Handlungen unter einander, uns selber oder anderen gegenüber oder gegenüber Tieren sind gegen dem Gesetz und werden gemäß dem schwedischem Gesetz beurteilt.
