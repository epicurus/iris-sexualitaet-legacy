## Sexualität in der Geschichte

<div class="handwritten">
Ja, ich kann dir soweit folgen, aber es fällt mir dennnoch schwer zu glauben, dass eine Beziehung genauso gut sein kann, wenn sie aus den Strategien anderer Menschen stammt, weil es so sein soll. Ich denke an alle Könige und andere hochbürtige Leute.
Es gab von ihnen ja Unmengen an Kindern im Dorf und im umliegenden Land, weil sie gezwungen waren gemäß Traditionen und Vereinbarungen zu heiraten. Und, ja, auch um den Frieden zwischen Ländern zu erhalten.
</div>

Ja, das stimmt, aber beachte, dass das eher ein Beweis für die Unabhängigkeit des Triebs in Bezug auf die Vernunft ist, als anders herum.
Diese Aristokraten, sowohl Männer als auch Frauen, wurden auf eine Weise erzogen, bei der es als primitiv galt, mehr attrahiert zu sein als das bloße Kurtesieren der Damen bei den Festen.
Im Übrigen waren Damen nur etwas, was man im Bett hatte und die die eigenen Kinder gebaren und sie daraufhin erzogen in gleicher Weise zu denken, wie es sich in diesen Kreisen ziemte.
Diese Anpassung führte dazu, dass man eine Verachtung gegenüber sich selber empfand weil man überhaupt über sich ergehen ließ, sich auf diese Weise anzupassen... und da bekam der Trieb ein Quell der Freiheit, gerade dies, auf eigenen Wegen sich vergnügen und sich ein Genüssliches verschaffen zu können. Der frommen Tradition zum Trotz, Liebhaberinnen nebenbei zu haben, und die Frauen ihrerseits Liebhaber, die durch die Geheimtür in ihr Gemach gelangten.

Es gab immer arme Frauen, die ihren Körper verkauft haben, und es gab immer arme, hübsche Jünglinge, die das gleiche taten, und die dafür kompensiert wurden.
Oft war die Situation der Frauen existenziell. Sie mussten dies tun, um zu überleben, und dabei war ihnen wohl bewusst, dass sie auf die Straße landen konnten, wenn sie denjenigen, der ihnen geneigt war nicht mehr bezauberten.
Die Schönheit erlischt mit der Zeit und sie wurden gegen schönere, jüngere Objekte ausgewechselt. Es sei denn, es gelang ihnen ihren Partner in eine symbiotische Beziehung hineinzuverwickeln, sodass der Partner es nicht aushalten konnte frei zu sein.

Im Falle der jungen armen schönen Männer, so ging es immer darum, dass sie sich in den Salons der Wohlhabenden bewegten, um diese wohlhabenden Damen zu begegnen, seien sie verheiratet oder unverheiratet, um mit ihnen in Beziehung zu kommen.
Die Frauen hatten immer ihre eigenen Zimmer und meistens gab es Geheimgänge. Man war der Ansicht, dass die Frauen die Möglichkeit haben sollten, Bedienung aus der Küche zu bekommen, wenn sie unpässlich im Bett waren, um zu frühstücken, um ein Bad zu bekommen und anderes. Aber oft wurden diese Geheimgänge fleißig durch Kavaliere benutzt, die den Damen den Hof machten.
Es war ein öffentliches Geheimnis, von dem man nicht laut sprach. Die Kirche verurteilte dies, aber oft waren diese Liebhaber Männer der Kirche.

Die verheirateten Frauen hatten nicht so große Schwierigkeiten, denn niemand kümmerte sich darum, wer der Vater ihrer Kinder war. Es war immer der Mann, der der Vater war, wer auch immer die Frau befruchtet hatte.
Schlimmer war es für eine Jungfrau. Sie musste schnell schauen, dass ihre Eltern sie vermählten und verheirateten, bevor man sehen konnte, dass sie schwanger war.
Aber wen sie dann heiratete, hat seine Gewohnheiten mit anderen Frauen nicht geändert, und so kümmerte er sich selten darum, dass sie schwanger war. Und außerdem hatten die Männer es gern, dass die Frau sexuelle Erfahrung hatte und es verstand wie sie tun sollte, um einen Mann im Bett zufriedenzustellen.
Dies galt für die oberen Stände. Aber es weitete sich auch zu den Bürgerfamilien aus, die ihren Status zeigte, indem sie Diener hatten, und dann ein öffentliches Leben und ein anderes privat hatten.

Außerhalb von diesem System waren die Bauern und die Armen. Sie lebten auf eine ganz andere Weise.
Oft wohngemeinschaftlich, eng zusammen, damit sie füreinander da waren, um die Wärme im Winter aufrechtzuerhalten .
Oft nahmen die Oberklasse und die Bürgerklasse sie in ihren Dienst und damit konnten sie überleben. Bei ihnen schaute man darauf, dass die Männer alle Frauen versorgen. Der Lohn für ihre Arbeit war Naturalien, Essen und Unterkunft und auch eine kleine Geldsumme für den Rest, was sie nötig hatten.

Die Frauen bekamen in der Tat nichts, außer dass sie bei ihren Männern wohnen durften und ihre Arbeit wurde natürlich als ein Teil der Arbeit ihres Mannes gerechnet.

In der Unterklasse war die Sexualität für das Paar aus praktischen Gründen reserviert. Ihnen fehlte der Luxus, sich Gefühlsspielen und Kurtesieren hingeben zu können. Sex war ein Vergnügen und ein Genuss, war eine Weile des sich Entspannens inmitten des harten Daseins am Rande der Existenz.

Zudem wollte die Oberklasse, dass die Unterklasse so viele Kinder wie möglich bekam, damit die die Arbeitstruppe nicht abnahm, weil Erwachsene früh wegstarben. Und deshalb wurde es das Los der Frauen, Kinder zu kriegen, und mit dem, was da war, sparsam umzugehen - wenn nun überhaupt etwas da war.

Die einzigen Verhütungsmittel, das da waren, war einmal so lange wie nur möglich zu stillen und am liebsten viele Kinder gleichzeitig, denn da sprangen keine neuen Eier. Da konnte man bis zu vier Jahren vom Kindergebären frei sein.
Die zweite Methode war unterbrochener Beischlaf. Das war der Religion nach Sünde, aber niemand konnte das nachprüfen und daher machten die Leute das trotzdem.
Wenn ein Paar ein schönes beiderseitiges Verhältnis hatten, war es oft so, dass der Mann es um seiner Frau Willen tat. Und das führte oft dazu, dass die Frau nicht mehr als drei oder vier Kinder bekam an Stelle von 7-10 wie sonst üblich.
