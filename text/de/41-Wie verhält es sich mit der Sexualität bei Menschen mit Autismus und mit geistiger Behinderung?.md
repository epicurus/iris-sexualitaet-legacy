## Wie verhält es sich mit der Sexualität bei Menschen mit Autismus und mit geistiger Behinderung?

<div class="handwritten">
Nächste Frage:
Wie verhält es sich mit der Sexualität bei Menschen mit Autismus und mit geistiger Behinderung?
</div>

Der Trieb wird sehr wenig beeinflusst durch Autismus und durch geistige Behinderung.
Im Grunde ist er freier bei ihnen, weil sie haben nicht eine Menge soziale Hemmnisse und Begrenzungen.
Deshalb wird der Trieb oft zum Problem für Eltern und für Pflegepersonal, weil sie möchten, dass der Typ normal gehemmt und zurückgezogen sein soll, auch bei den Menschen mit Entwicklungsvariationen.
Damit ist er natürlich, aber braucht die Regulierung, und das wird derjenige oder diejenige nicht verstehen.
Aber manchmal ist es möglich, neu zu programmieren, und in ein anderes Verhalten umzuwandeln an der Stelle, manchmal.
